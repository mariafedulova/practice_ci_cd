FROM python:3.12

ADD main.py .

ENTRYPOINT [ "python", "main.py" ]