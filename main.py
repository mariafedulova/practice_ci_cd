import random
import sys

arr = []
for i in range(100):
    arr.append(random.randint(1, 100))
arr.sort()


def binary_search(data, elem):

    low = 0
    high = len(data) - 1

    while low <= high:
        middle = (low + high)//2
        if data[middle] == elem:
            return middle
        elif data[middle] > elem:
            high = middle - 1
        else:
            low = middle + 1

    return -1


print(binary_search(arr, int(sys.argv[1])))
